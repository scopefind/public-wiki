## Welcome ##

Scopefind is a student project aimed on creating a service for automation of processes for IT recruiting.
We use powerful analytics for resume gathering and catalogization. Scopefind uses capabilities of 
open-source community (GitHub, BitBucket) to find perfect candidates for your needs.

## Team ##

*  Grigory Nitsenko
*  Alexander Prendota
*  Sergey Brezhnev
*  Vladimir Plokhotnichenko
*  Vlad Fedorenkov
*  Kirill Kashirin